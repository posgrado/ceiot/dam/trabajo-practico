import { Component } from '@angular/core';
import { Device } from '../model/Device';
import { DeviceService } from '../services/device.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  deviceList: Device[];

  constructor(private deviceService: DeviceService) {
    this.deviceService.getDevices().then((devices) => {
      this.deviceList = devices;
    })
  }

}
