import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IrrigationsPageRoutingModule } from './irrigations-routing.module';

import { IrrigationsPage } from './irrigations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IrrigationsPageRoutingModule
  ],
  declarations: [IrrigationsPage]
})
export class IrrigationsPageModule {}
