import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Measure } from '../model/Measure';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {
  urlBack = 'http://localhost:8000/api/'

  constructor(private _http: HttpClient) { }

  getLastMeasureByDeviceId(id): Promise<Measure> {
    return this._http.get(this.urlBack + 'measures/device/' + id + '/last').toPromise().then((measures: Measure[]) => {
      console.log('Response from measures API: ', measures);
      return measures[0];
    });
  }

  getMeasuresByDeviceId(id): Promise<Measure[]> {
    return this._http.get(this.urlBack + 'measures/device/' + id).toPromise().then((measures: Measure[]) => {
      console.log('Response from measures API: ', measures);
      return measures;
    })
  }
}
