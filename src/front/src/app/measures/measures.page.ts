import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Device } from '../model/Device';
import { Measure } from '../model/Measure';
import { DeviceService } from '../services/device.service';
import { MeasureService } from '../services/measure.service';

@Component({
  selector: 'app-measures',
  templateUrl: './measures.page.html',
  styleUrls: ['./measures.page.scss'],
})
export class MeasuresPage implements OnInit {

  public deviceId: number;
  public measures: Measure[];
  public device: Device;
  public nomDisp = '';
  constructor(
    private Arouter: ActivatedRoute,
    private deviceService: DeviceService,
    private measureService: MeasureService
  ) {
    this.deviceId = Number(this.Arouter.snapshot.paramMap.get('id'))
  }

  ngOnInit() {
    this.deviceService.getDeviceById(this.deviceId).then(
      (device: Device) => {
        this.device = device;
        console.log('Device got from API: ', device);
      })
    this.measureService.getMeasuresByDeviceId(this.deviceId).then((measures: Measure[]) => {
      this.measures = measures;
      console.log('Measures got from API: ', measures);
    })
  }

}
