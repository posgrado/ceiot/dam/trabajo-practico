# Proyecto final DAM
Este proyecto permite:
* Visualizar una lista de dispositivos.
* Ver la última medición en un gráfico.
* Abrir o cerrar una electroválvula.
* Ver todas las mediciones.
* Ver log de riego.

Author:

* David Broin

## Prerequisitos
* docker
* docker-compose

## Instrucciones de uso
* Ejecutar
```sh
docker-compose up
```
**NOTA:** Buen momento para un ☕️, es normal que la primera vez tome bastante tiempo en levantar. En mi caso tarda aproximadamente **10 minutos!**.

* En el navegador abrir http://localhost:8100

## Licencia

This project is published under GPLV3+ licence.
