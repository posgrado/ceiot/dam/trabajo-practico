import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Device } from '../model/Device';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  urlBack = 'http://localhost:8000/api/'

  constructor(private _http: HttpClient) { }

  getDevices(): Promise<Device[]> {
    return this._http.get(this.urlBack + 'devices').toPromise().then(
      (devices: Device[]) => {
        return devices;
      });
  }

  getDeviceById(id): Promise<Device> {
    return this._http.get(this.urlBack + 'devices/' + id).toPromise().then(
      (devices: Device[]) => {
        console.log('Response from device ' + id + ' API: ', devices);
        return devices[0];
      });
  }
}
