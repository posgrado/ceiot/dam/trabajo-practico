import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import * as Highcharts from 'highcharts'
import { Device } from '../model/Device'
import { Irrigation } from '../model/Irrigation'
import { Measure } from '../model/Measure'
import { DeviceService } from '../services/device.service'
import { IrrigationService } from '../services/irrigation.service'
import { MeasureService } from '../services/measure.service'

declare var require: any
require('highcharts/highcharts-more')(Highcharts)
require('highcharts/modules/solid-gauge')(Highcharts)

@Component({
  selector: 'app-device',
  templateUrl: './device.page.html',
  styleUrls: ['./device.page.scss'],
})
export class DevicePage implements OnInit {

  private value: number = 0
  public myChart
  private chartOptions


  public device: Device
  public deviceId: string
  public measure: Measure

  public solenoidState = false
  constructor(
    private Arouter: ActivatedRoute,
    private irrigationService: IrrigationService,
    private deviceService: DeviceService,
    private measureService: MeasureService
  ) {
    this.getDeviceAndMeasures()
  }

  ngOnInit() {
  }

  getDeviceAndMeasures() {
    this.deviceId = this.Arouter.snapshot.paramMap.get('id');

    this.deviceService.getDeviceById(this.deviceId).then(
      (device) => {
        this.device = device;
        console.log('Device got from API: ', this.device);
      }
    );

    this.measureService.getLastMeasureByDeviceId(this.deviceId).then((measure: Measure) => {
      this.measure = measure;
      console.log('Measure got from API: ' + measure);

      this.value = Number(this.measure.valor);

      this.generateChart();

    });
  }

  generateChart() {
    this.chartOptions = {
      chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
      }
      , title: {
        text: [this.device.nombre]
      }

      , credits: { enabled: false }


      , pane: {
        startAngle: -150,
        endAngle: 150
      }
      // the value axis
      , yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
          step: 2,
          rotation: 'auto'
        },
        title: {
          text: 'kPA'
        },
        plotBands: [{
          from: 0,
          to: 10,
          color: '#55BF3B' // green
        }, {
          from: 10,
          to: 30,
          color: '#DDDF0D' // yellow
        }, {
          from: 30,
          to: 100,
          color: '#DF5353' // red
        }]
      }
      ,

      series: [{
        name: 'kPA',
        data: [this.value],
        tooltip: {
          valueSuffix: ' kPA'
        }
      }]

    };
    this.myChart = Highcharts.chart('highcharts', this.chartOptions);
  }



  openSolenoid() {
    const current_datetime = new Date();
    const formatted_date = current_datetime.getFullYear() + '-' + (current_datetime.getMonth() + 1) + '-' + current_datetime.getDate() + ' ' + current_datetime.getHours() + ':' + current_datetime.getMinutes() + ':' + current_datetime.getSeconds();

    if (!this.solenoidState) {
      const irrigation: Irrigation = new Irrigation(99, 1, formatted_date, this.device.electrovalvulaId);
      this.irrigationService.postIrrigation(irrigation);
    } else {
      const irrigation: Irrigation = new Irrigation(99, 0, formatted_date, this.device.electrovalvulaId);
      this.irrigationService.postIrrigation(irrigation);
    }
    this.solenoidState = !this.solenoidState;
  }
}
