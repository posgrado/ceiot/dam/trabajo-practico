//=======[ Settings, Imports & Data ]==========================================

var PORT = 3000

var express = require('express')
var cors = require('cors')
var app = express()

var deviceRouter = require('./routes/device');
var irrigationRouter = require('./routes/irrigation');
var measureRouter = require('./routes/measure');

//Configure CORS
var corsOption = { origin: 'http://localhost:8100', optionSucessStatus: 200 };
app.use(cors(corsOption));

// to parse application/json
app.use(express.json())
app.use('/api/devices', deviceRouter);
app.use('/api/irrigations', irrigationRouter);
app.use('/api/measures', measureRouter);

//=======[ Main module code ]==================================================
app.listen(PORT, function(req, res) {
    console.log("NodeJS API running correctly")
})

//=======[ End of file ]=======================================================