var express = require('express');
var measureRouter = express.Router();
var pool = require('../../mysql');

measureRouter.post('/', function(req, res, next) {
    pool.query('INSERT INTO Mediciones (fecha, valor, dispositivoId) VALUES (?, ?, ?)', [req.body.fecha, req.body.valor, req.body.dispositivoId], function (err, result, fields) {
        if (err) {
            res.send(err).status(400);
            return;
        }
        res.send(result);
    });
})

measureRouter.get('/device/:id', function(req, res, next) {
    pool.query('SELECT * FROM Mediciones WHERE dispositivoId=? ORDER BY fecha DESC', [req.params.id], function (err, result, fields) {
        if (err) {
            res.send(err).status(400);
            return;
        }
        res.send(result);
    });
})

measureRouter.get('/device/:id/last', function(req, res, next) {
    pool.query('SELECT * FROM Mediciones WHERE dispositivoId=? ORDER BY fecha DESC LIMIT 1', [req.params.id], function (err, result, fields) {
        if (err) {
            res.send(err).status(400);
            return;
        }
        res.send(result);
    });
})

module.exports = measureRouter