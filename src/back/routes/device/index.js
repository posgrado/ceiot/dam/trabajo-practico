var express = require('express');
var deviceRouter = express.Router();
var pool = require('../../mysql');

deviceRouter.get('/', function(req, res, next) {
    pool.query('SELECT * FROM Dispositivos', function (err, result, fields) {
        if (err) {
            res.send(err).status(400);
            return;
        }
        res.send(result);
    });
})

deviceRouter.get('/:id', function(req, res, next) {
    pool.query('SELECT * FROM Dispositivos WHERE dispositivoId=?', [req.params.id], function (err, result, fields) {
        if (err) {
            res.send(err).status(400);
            return;
        }
        res.send(result);
    });
})

module.exports = deviceRouter