import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Irrigation } from '../model/Irrigation';
import { Measure } from '../model/Measure';

@Injectable({
  providedIn: 'root'
})
export class IrrigationService {
  
  urlBack = 'http://localhost:8000/api/'
  
  constructor(private _http: HttpClient) { }
  
  getIrrigationsBySolenoidId(id: number): Promise<Irrigation[]> {
    return this._http.get(this.urlBack + 'irrigations/solenoid/' + id).toPromise().then((irrigations: Irrigation[]) => {
      console.log('Response from irrigations API: ', irrigations)
      return irrigations;
    })
  }

  postIrrigation(irrigation: Irrigation) {
    return this._http.post(this.urlBack + 'irrigations',
      {
        apertura: irrigation.apertura,
        fecha: irrigation.fecha,
        electrovalvulaId: irrigation.electrovalvulaId,
      }).toPromise().then((result) => {
        return result;
      });
  }
}
