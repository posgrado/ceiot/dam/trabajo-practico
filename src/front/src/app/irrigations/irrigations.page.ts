import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Device } from '../model/Device';
import { Irrigation } from '../model/Irrigation';
import { DeviceService } from '../services/device.service';
import { IrrigationService } from '../services/irrigation.service';

@Component({
  selector: 'app-irrigations',
  templateUrl: './irrigations.page.html',
  styleUrls: ['./irrigations.page.scss'],
})
export class IrrigationsPage implements OnInit {


  public deviceId: number
  public device: Device
  public irrigations: Irrigation[]

  constructor(
    private Arouter: ActivatedRoute,
    private deviceService: DeviceService,
    private irrigationService: IrrigationService
  ) {
    this.deviceId = Number(this.Arouter.snapshot.paramMap.get('id'))
  }

  ngOnInit() {
    this.deviceService.getDeviceById(this.deviceId).then(
      (device: Device) => {
        this.device = device;
        console.log('Device got from API: ', device);
        this.irrigationService.getIrrigationsBySolenoidId(device.electrovalvulaId).then((irrigations: Irrigation[]) => {
          this.irrigations = irrigations
          console.log('Irrigation log got from API: ', irrigations)
        })
      })
  }

}
