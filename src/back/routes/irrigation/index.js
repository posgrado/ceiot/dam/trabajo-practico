var express = require('express');
var irrigationRouter = express.Router();
var pool = require('../../mysql');

irrigationRouter.post('/', function(req, res, next) {
    pool.query('INSERT INTO Log_Riegos (apertura, fecha, electrovalvulaId) VALUES (?, ?, ?)', [req.body.apertura, req.body.fecha, req.body.electrovalvulaId], function (err, result, fields) {
        if (err) {
            res.send(err).status(400);
            return;
        }
        res.send(result);
    });
})

irrigationRouter.get('/solenoid/:id', function(req, res, next) {
    pool.query('SELECT * FROM Log_Riegos WHERE electrovalvulaId=? ORDER BY fecha DESC', [req.params.id], function (err, result, fields) {
        if (err) {
            res.send(err).status(400);
            return;
        }
        res.send(result);
    });
})

module.exports = irrigationRouter