import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IrrigationsPage } from './irrigations.page';

const routes: Routes = [
  {
    path: '',
    component: IrrigationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IrrigationsPageRoutingModule {}
