import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IrrigationsPage } from './irrigations.page';

describe('IrrigationsPage', () => {
  let component: IrrigationsPage;
  let fixture: ComponentFixture<IrrigationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IrrigationsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IrrigationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
